FROM caddy:2.1.1-alpine
ADD dist /dist
WORKDIR /dist
CMD ["caddy file-server"]
