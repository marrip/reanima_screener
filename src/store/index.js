import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    category: "Ulriken",
    registered: false
  },
  mutations: {
    chosenCategory(state, category) {
      state.category = category;
    },
    register(state, registered) {
      state.registered = registered;
    }
  },
  actions: {},
  modules: {}
});
