import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: {
      name: "Home"
    }
  },
  {
    path: "/registration",
    name: "Registration",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("../views/Home.vue")
  },
  {
    path: "/screen",
    name: "Screen",
    component: () => import("../views/Screen.vue")
  }
];

const router = new VueRouter({
  routes: routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

export default router;
